const palette = {
  black: '#17181A',
  blue: '#1140E6',
  blueDark: '#0112D8',
  greyDark: '#484D53',
  knockedBlue: '#DBE3FF',
  pink: '#FFDBE1',
  white: 'white',
};

const colorModes = {
  default: {
    background: palette.white,
    muted: palette.pink,
    text: palette.black,

    // Special components
    heading: palette.blue,
    logomark: palette.greyDark,
    shouting: palette.blue,
  },
  options: {
    darkBlue: {
      background: palette.blueDark,
      muted: palette.blue,
      text: palette.knockedBlue,

      // Special components
      heading: palette.white,
      logomark: palette.white,
      shouting: palette.white,
    },
    blue: {
      background: palette.blue,
      muted: palette.pink,
      text: palette.knockedBlue,

      // Special components
      heading: palette.white,
      logomark: palette.white,
      shouting: palette.white,
    },
    pink: {
      background: palette.pink,
      muted: palette.blue,
      text: palette.greyDark,

      // Special components
      heading: palette.blue,
      logomark: palette.white,
      shouting: palette.white,
    },
  },
};

const theme = {
  fonts: {
    body: 'Baton Turbo, "Open Sans", sans-serif',
    heading: 'Brando, Georgia, serif',
    monospace: 'Corrier, monospace',
  },
  fontWeights: {
    body: 400,
    heading: 600,
  },
  colors: {
    ...palette,
    ...colorModes.default,
    modes: colorModes.options,
  },
  textStyles: {
    shouting: {
      textTransform: 'uppercase',
      fontFamily: 'body',
      color: 'shouting',
      letterSpacing: '0.1875em',
      textAlign: 'center',
      lineHeight: 1.45,
      fontSize: '6.25vw',
      zIndex: 1,
    },
    small: {
      fontSize: 2,
    },
  },
  styles: {
    h1: {
      color: 'heading',
      mb: 0,
    },
  },
};

export default theme;
