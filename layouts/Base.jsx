/** @jsx jsx */
import { jsx, useColorMode } from 'theme-ui';
import React from 'react';
import { useDeck } from 'mdx-deck';
import { GlobalStyles, Breadcrumb, ShapesFooter, Logo } from '../components';

const Base = ({
  breadcrumb = [],
  colorVariant = 'default',
  withLogo = true,
  children,
}) => {
  const [mode, setMode] = useColorMode();
  const deck = useDeck();

  React.useEffect(() => {
    if (mode !== colorVariant) {
      setMode(colorVariant);
    }
  });

  return (
    <>
      <GlobalStyles />
      <Breadcrumb trail={breadcrumb} />
      {withLogo && <Logo />}
      {children}

      <ShapesFooter numPages={deck.length} currentIndex={deck.index} />
    </>
  );
};

export default Base;
