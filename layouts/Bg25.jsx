/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import Symbol from '../components/Symbol2';
import Base from './Base';

const Bg25 = ({ children, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          zIndex: 1,
          maxWidth: '25em',
          mr: [0, '45vw'],
          width: 'auto',
        }}
      >
        <div
          sx={{
            mx: 5,
          }}
        >
          {children}
        </div>
      </div>
      <div
        sx={{
          position: 'fixed',
          right: '0',
          width: [800, 1170],
          mr: ['-8em', '-12em'],
          zIndex: 0,
        }}
      >
        <Symbol />
      </div>
    </Base>
  );
};

export default Bg25;
