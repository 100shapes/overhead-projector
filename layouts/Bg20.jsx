/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import Symbol from '../components/Symbol1';
import Base from './Base';

const Bg20 = ({ children, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          position: 'fixed',
          left: '0',
          width: ['calc(30rem + 18vw)', 'calc(20rem + 18vw)'],
          ml: ['-10em', '-2em'],
          zIndex: 0,
        }}
      >
        <Symbol />
      </div>

      <div
        sx={{
          zIndex: 1,
          maxWidth: '25em',
          ml: [0, '34vw'],
          width: 'auto',
        }}
      >
        <div
          sx={{
            margin: '1em',
          }}
        >
          {children}
        </div>
      </div>
    </Base>
  );
};

export default Bg20;
