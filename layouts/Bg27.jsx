/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Symbol from '../components/Symbol4';
import Base from './Base';

const Bg27 = ({ heading, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          position: 'absolute',
          width: '130%',
          mt: '2%',
          zIndex: 0,
        }}
      >
        <Symbol />
      </div>
      <Styled.h1
        sx={{
          variant: 'textStyles.shouting',
        }}
      >
        {heading}
      </Styled.h1>
    </Base>
  );
};

export default Bg27;
