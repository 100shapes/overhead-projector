/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Symbol from '../components/Symbol3';
import Base from './Base';

const Bg10 = ({ heading, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          position: 'absolute',
          width: [1200, '134vw'],
          left: ['-12%', '10%'],
          zIndex: 0,
        }}
      >
        <Symbol />
      </div>
      <div
        sx={{
          mx: 5,
          zIndex: 1,
        }}
      >
        <Styled.h1
          sx={{
            variant: 'textStyles.shouting',
          }}
        >
          {heading}
        </Styled.h1>
      </div>
    </Base>
  );
};

export default Bg10;
