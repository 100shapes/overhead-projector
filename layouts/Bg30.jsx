/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import Symbol from '../components/Symbol5';
import Base from './Base';

const Bg30 = ({ children, ...props }) => {
  return (
    <Base {...props}>
      <div
        sx={{
          zIndex: 1,
          maxWidth: '25em',
          mr: [0, '45vw'],
          width: 'auto',
        }}
      >
        <div
          sx={{
            mx: 5,
          }}
        >
          {children}
        </div>
      </div>
      <div
        sx={{
          position: 'fixed',
          right: -2200,
          mt: -480,
          width: [800, 3178],
          zIndex: 0,
        }}
      >
        <Symbol />
      </div>
    </Base>
  );
};

export default Bg30;
