/** @jsx jsx */
import { jsx } from 'theme-ui';
import useMutedColor from '../hooks/useMutedColor';
import React from 'react';

const Symbol5 = ({ bg }) => {
  const fill = useMutedColor(bg);

  return (
    <svg fill='none' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3909 2191'>
      <path
        fill={fill}
        d='M331.35 2190.66L.007 1214.946 3577.625.02l331.344 975.715z'
      />
    </svg>
  );
};

export default Symbol5;
