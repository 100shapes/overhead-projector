export { default as Breadcrumb } from './Breadcrumb';
export { default as GlobalStyles } from './GlobalStyles';
export { default as Logo } from './Logo';
export { default as LottiePlayer } from './LottiePlayer';
export { default as ShapesFooter } from './ShapesFooter';
