/** @jsx jsx */
import { jsx } from 'theme-ui';
import useMutedColor from '../hooks/useMutedColor';
import React from 'react';

const Symbol3 = ({ bg }) => {
  const fill = useMutedColor(bg);

  return (
    <svg fill='none' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 2569 3285'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M1563.92 3284.11c554.66 0 1004.3-449.64 1004.3-1004.3 0-554.67-449.64-1004.31-1004.3-1004.31S559.617 1725.14 559.617 2279.81c0 554.66 449.643 1004.3 1004.303 1004.3zm0-547.8c252.12 0 456.5-204.39 456.5-456.5 0-252.12-204.38-456.51-456.5-456.51s-456.5 204.39-456.5 456.51c0 252.11 204.38 456.5 456.5 456.5z'
        fill={fill}
      />
      <path
        fill={fill}
        d='M423.971 1901.45L0 1554.557 1271.938 0l423.97 346.893z'
      />
    </svg>
  );
};

export default Symbol3;
