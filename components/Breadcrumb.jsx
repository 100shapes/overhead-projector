/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import Header from '@mdx-deck/gatsby-plugin/src/header';
import intersperse from '../helpers/intersperse';

const Breadcrumb = ({ trail: parts }) => {
  const trail = intersperse(parts, ' • ');

  return (
    <Header>
      <div
        sx={{
          position: 'fixed',
          top: 36,
          left: 5,
        }}
      >
        <Styled.p
          sx={{
            fontSize: 4,
            m: 0,
          }}
        >
          {trail}
        </Styled.p>
      </div>
    </Header>
  );
};

export default Breadcrumb;
