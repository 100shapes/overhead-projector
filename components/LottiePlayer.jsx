/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';

const LottiePlayer = ({ src }) => {
  return (
    <div
      sx={{
        width: '80%',
        height: '90%',
      }}
    >
      <lottie-player
        src={src}
        background='transparent'
        speed='1'
        loop
        autoplay
      />
    </div>
  );
};

export default LottiePlayer;
