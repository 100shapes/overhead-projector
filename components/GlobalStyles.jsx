/** @jsx jsx */
import { jsx } from 'theme-ui';
import React from 'react';
import { Global, css } from '@emotion/core';
import '../assets/fonts/fonts.css';

const GlobalStyles = () => (
  <Global
    styles={css`
      div,
      body {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        transition: background-color 0.6s cubic-bezier(0.19, 1, 0.22, 1);
      }
    `}
  />
);

export default GlobalStyles;
