/** @jsx jsx */
import { jsx, Styled } from 'theme-ui';
import React from 'react';
import intersperse from '../helpers/intersperse';

const ShapesFooter = ({ numPages, currentIndex }) => {
  const currentPageDisplay = currentIndex + 1;
  const pageNumbers = intersperse([currentPageDisplay, numPages], ' / ');
  return (
    <>
      <div
        sx={{
          position: 'fixed',
          bottom: 4,
          left: 5,
        }}
      >
        <Styled.p
          sx={{
            variant: 'textStyles.small',
          }}
        >
          Copyright &copy; {new Date().getFullYear()} 100 Shapes Ltd. All rights
          reserved.
        </Styled.p>
      </div>
      <div
        sx={{
          position: 'fixed',
          bottom: 4,
          right: 5,
        }}
      >
        <Styled.p
          sx={{
            variant: 'textStyles.small',
          }}
        >
          {pageNumbers}
        </Styled.p>
      </div>
    </>
  );
};

export default ShapesFooter;
